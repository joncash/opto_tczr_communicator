﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace OPTO
{
	/// <summary>
	/// OPTO TCZR Controller
	/// <para>Use TCZRManager would be good</para>
	/// </summary>
	public class TCZR
	{
		/// <summary>
		/// 開啟通訊
		/// </summary>
		/// <param name="PortNumber">1=COM1, 2=COM2 etc... max 99</param>
		/// <param name="Baud">1200, 2400,4800,9600,19200,38400,57600,115200</param>
		/// <returns>0 = OK, -1 = Error</returns>
		[DllImport("Revolver_Dll.dll")]
		public static extern int InitComm(int PortNumber, int Baud);


		/// <summary>
		/// 關閉通訊
		/// </summary>
		[DllImport("Revolver_Dll.dll")]
		public static extern void CloseComm();

		/// <summary>
		/// 取得 DLL 版號
		/// </summary>
		/// <returns>version string</returns>
		[DllImport("Revolver_Dll.dll")]
		public static extern string GetDllVersion();

		/// <summary>
		/// **設定 Command Timeout**
		/// </summary>
		/// <param name="milliseconds"> 0 ~5000, default 500 </param>
		/// <returns>0 = OK, -1 , -2 = Error</returns>
		[DllImport("Revolver_Dll.dll")]
		public static extern int SetTimeOutRxCommand(int milliseconds);

		/// <summary>
		/// ** 取得韌體版本**
		/// <para> 0= OK, -1 = timeout error, -2 = checksum error, -3 = communication not started</para>
		/// </summary>
		/// <param name="Version"></param>
		/// <returns></returns>
		[DllImport("Revolver_Dll.dll")]
		public static extern int GetFwVersion(string Version);

		/// <summary>
		/// **取得目前狀態**
		/// </summary>
		/// <param name="Position">焦段位置</param>
		/// <param name="ErrorOverSupply">1= 電壓太高, 0= OK</param>
		/// <param name="ErrorUnderSupply">1= 電壓太低, 0= OK</param>
		/// <param name="ErrorOverCurrent">1= 電流太高, 0=OK</param>
		/// <param name="ErrorI2T">系統自我保護狀態 1= Error, 0=OK</param>
		/// <param name="ErrorPosition">1= Error, 0=OK</param>
		/// <param name="WarningPosition">1 = Warning, reached with more steps than StepPulse, 0=OK</param>
		/// <param name="StepDone">回傳步數</param>
		/// <param name="Busy">1= Busy, 0 = OK</param>
		/// <param name="Run">1 = Moving, 0 = OK</param>
		/// <returns>0 = OK, -1=Timeout, -2=Checksum Error, -3 = Communication not started</returns>
		[DllImport("Revolver_Dll.dll")]
		public static extern int GetState(ref byte Position, ref byte ErrorOverSupply,
										ref byte ErrorUnderSupply, ref byte ErrorOverCurrent,
										ref byte ErrorI2T, ref byte ErrorPosition,
										ref byte WarningPosition, ref byte StepDone,
										ref byte Busy, ref byte Run);


		/// <summary>
		/// **取得參數
		/// </summary>
		/// <param name="T1">Pulse : 0~10000 ms</param>
		/// <param name="T2">Pulse : 0~10000 ms</param>
		/// <param name="T3">Pulse : 0~10000 ms</param>
		/// <param name="TDuty">0~2: 0=25%, 1=50%, 2=75%</param>
		/// <param name="TT1">Special Pulse(Trillo) : 0~10000 ms</param>
		/// <param name="TT2">Special Pulse(Trillo) : 0~10000 ms</param>
		/// <param name="TT3">Special Pulse(Trillo) : 0~10000 ms</param>
		/// <param name="TTDuty">0~2: 0=25%, 1=50%, 2=75%</param>
		/// <param name="I2Tmax">0~ 1000000 Thermal image limit</param>
		/// <param name="VinMin">0~ 60000 mV</param>
		/// <param name="VinMax">0~ 60000 mV</param>
		/// <param name="Imax">0~ 60000 mA</param>
		/// <param name="StepPulse">0~200, pluses needed to move to a position</param>
		/// <param name="ExtraPulse">0~200 Extra pulses</param>
		/// <returns>0=OK, -1 = timeout, -2=checksum error, -3= communication not started</returns>
		[DllImport("Revolver_Dll.dll")]
		public static extern int GetParameters(ref int T1, ref int T2, ref int T3, ref int TDuty,
												ref int TT1, ref int TT2, ref int TT3, ref int TTDuty,
												ref int I2Tmax, ref int VinMin, ref int VinMax,
												ref int Imax, ref int StepPulse, ref int ExtraPulse);


		/// <summary>
		/// **變焦
		/// </summary>
		/// <param name="position">0,1,2,3</param>
		/// <returns>0=OK, -1=Timeout, -2=Checksum Error, -3 = NAK device busy, -4 = communication is not open</returns>
		[DllImport("Revolver_Dll.dll")]
		public static extern int GoToPosition(int position);

		/// <summary>
		/// ** Special Signal
		/// </summary>
		/// <returns>0=OK, -1 = timeout error -2 =checksum error -3= NAK device busy, -4 = communication is not open</returns>
		[DllImport("Revolver_Dll.dll")]
		public static extern int Trillo();


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		[DllImport("Revolver_Dll.dll")]
		public static extern string DebugBufferTx();

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		[DllImport("Revolver_Dll.dll")]
		public static extern string DebugBufferRx();
	}
}
