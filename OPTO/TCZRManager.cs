﻿using OPTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OPTO
{
	/// <summary>
	/// TCZR manager
	/// <para>TCZR wrapper</para>
	/// </summary>
	public class TCZRManager
	{
		/// <summary>
		/// possible values : 1-99
		/// <para>**Default : 12</para>
		/// </summary>
		private int _portNum = 12;

		/// <summary>
		/// possible values : 1200, 2400,4800,9600,19200,38400,57600,115200
		/// <para>**Default : 115200</para>
		/// </summary>
		private int _baudrate = 115200;

		public bool Connected
		{
			get
			{
				var model = GetState();
				return model.State == "OK";
			}
		}

		#region 建構子===================================================

		/// <summary>
		/// 建構子
		/// </summary>
		/// <param name="portNum"></param>
		/// <param name="baudrate"></param>
		public TCZRManager(int portNum, int baudrate)
		{
			_portNum = portNum;
			_baudrate = baudrate;
		}
		public TCZRManager()
		{

		}
		#endregion



		#region public functions ========================================

		/// <summary>
		/// 設定通訊埠
		/// </summary>
		/// <param name="portNum">possible values : 1-99</param>
		public void SetPortNumber(int portNum)
		{
			_portNum = portNum;
		}

		/// <summary>
		/// 設定通訊速度 (Baudrate)
		/// </summary>
		/// <param name="baudrate">posible values : 1200, 2400,4800,9600,19200,38400,57600,115200</param>
		public void SetBaudRate(int baudrate)
		{
			_baudrate = baudrate;
		}

		/// <summary>
		/// 連接通訊
		/// </summary>
		/// <returns>True = OK, False = Error</returns>
		public bool InitComm()
		{
			var val = TCZR.InitComm(_portNum, _baudrate);
			// 0 = OK
			var commOK = (val == 0);
			var stateModel = GetState();
			var stateOK = (stateModel.State == "OK");
			return commOK && stateOK;
		}

		/// <summary>
		/// 關閉通訊
		/// </summary>
		/// <returns></returns>
		public void CloseComm()
		{
			TCZR.CloseComm();
		}

		/// <summary>
		/// 變換焦段
		/// <para>0~3</para>
		/// </summary>
		/// <param name="position">0~3</param>
		/// <returns></returns>
		public string GoToPosition(int position)
		{
			var state = "";
			if (position >= 0 && position <= 3)
			{
				var val = TCZR.GoToPosition(position);
				state = getStatusByReturnValue2(val);
			}
			return state;
		}

		/// <summary>
		/// 取得 TCZR 狀態
		/// </summary>
		/// <returns></returns>
		public TCZRState GetState()
		{
			byte Position = 0, ErrorOverSupply = 0, ErrorUnderSupply = 0, ErrorOverCurrent = 0, ErrorI2T = 0, ErrorPosition = 0
				, WarningPosition = 0, StepDone = 0, Busy = 0, Run = 0;
			var val = TCZR.GetState(ref Position, ref ErrorOverSupply, ref ErrorUnderSupply, ref ErrorOverCurrent,
				ref ErrorI2T, ref ErrorPosition, ref WarningPosition, ref StepDone, ref Busy, ref Run);

			return new TCZRState()
			{
				Position = Position,
				ErrorOverSupply = ErrorOverSupply,
				ErrorUnderSupply = ErrorUnderSupply,
				ErrorOverCurrent = ErrorOverCurrent,
				ErrorI2T = ErrorI2T,
				ErrorPosition = ErrorPosition,
				WarningPosition = WarningPosition,
				StepDone = StepDone,
				Busy = Busy,
				Run = Run,
				State = getStatusByReturnValue(val),
			};
		}

		/// <summary>
		/// 取得 TCZR dll version
		/// </summary>
		/// <returns></returns>
		public string GetDllVersion()
		{
			return TCZR.GetDllVersion();
		}

		/// <summary>
		/// 取得 Fireware version
		/// </summary>
		/// <returns></returns>
		public string GetFwVersion()
		{
			var dllVer = TCZR.GetDllVersion();
			var val = TCZR.GetFwVersion(dllVer);
			return getStatusByReturnValue(val);
		}

		/// <summary>
		/// 取得動作中的訊號參數們
		/// </summary>
		/// <returns></returns>
		public TCZRParameters GetParameters()
		{
			var stateModel = GetState();
			TCZRParameters parameters = null;
			if (stateModel.Run == 1)
			{

				int T1 = -1, T2 = -1, T3 = -1, TDuty = -1,
					TT1 = -1, TT2 = -1, TT3 = -1, TTDuty = -1,
					I2Tmax = -1, VinMin = -1, VinMax = -1, Imax = -1,
					StepPulse = -1, ExtraPulse = -1;

				var val = TCZR.GetParameters(ref T1, ref T2, ref T3, ref TDuty,
					ref TT1, ref TT2, ref TT3, ref TTDuty,
					ref I2Tmax,
					ref VinMin, ref VinMax, ref Imax,
					ref StepPulse, ref ExtraPulse);

				var status = getStatusByReturnValue(val);
				parameters = new TCZRParameters()
				{
					T1 = T1,
					T2 = T2,
					T3 = T3,
					TDuty = TDuty,
					TT1 = TT1,
					TT2 = TT2,
					TT3 = TT3,
					TTDuty = TTDuty,
					I2Tmax = I2Tmax,
					VinMin = VinMin,
					VinMax = VinMax,
					Imax = Imax,
					StepPulse = StepPulse,
					ExtraPulse = ExtraPulse,
					State = status,
				};
			}
			return parameters;
		}

		/// <summary>
		/// 取得特殊信號 (Trigger)
		/// </summary>
		/// <returns></returns>
		public string Trillo()
		{
			var val = TCZR.Trillo();
			return getStatusByReturnValue2(val);
		}

		/// <summary>
		/// 設定接收Timeout 時間
		/// <para>** Default : 500</para>
		/// </summary>
		/// <param name="milliseconds"> 0 ~5000</param>
		/// <returns></returns>
		public string SetTimeOutRxCommand(int milliseconds)
		{
			var val = TCZR.SetTimeOutRxCommand(milliseconds);
			return (val == 0) ? "OK" : "NG";
		}

		/// <summary>
		/// 回傳送出訊號
		/// </summary>
		/// <returns></returns>
		public string DebugBufferTx()
		{
			return TCZR.DebugBufferTx();
		}

		/// <summary>
		/// 回傳接收訊號
		/// </summary>
		/// <returns></returns>
		public string DebugBufferRx()
		{
			return TCZR.DebugBufferRx();
		}
		#endregion

		/// <summary>
		/// TCZR command return value
		/// <para>TCZR.GetFwVersion(string)</para>
		/// <para>TCZR.GetParameters(params)</para>
		/// <para>TCZR.GetState(params)</para>
		/// </summary>
		/// <param name="val"></param>
		/// <returns></returns>
		private string getStatusByReturnValue(int val)
		{
			var status = "";
			switch (val)
			{
				case 0:
					status = "OK";
					break;
				case -1:
					status = "Timeout";
					break;
				case -2:
					status = "Checksum error";
					break;
				case -3:
					status = "Communication not started";
					break;
			}
			return status;
		}

		/// <summary>
		/// TCZR return value
		/// <para>TCZR.Trillo()</para>
		/// <para>TCZR.GoToPosition(int)</para>
		/// </summary>
		/// <param name="val"></param>
		/// <returns></returns>
		private string getStatusByReturnValue2(int val)
		{
			var status = "";
			switch (val)
			{
				case 0:
					status = "OK";
					break;
				case -1:
					status = "Timeout";
					break;
				case -2:
					status = "Checksum error";
					break;
				case -3:
					status = "NAK device busy";
					break;
				case -4:
					status = "Communication not started";
					break;
			}
			return status;
		}
	}
}
