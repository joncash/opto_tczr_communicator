﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OPTO.Models
{
	/// <summary>
	/// TCZR 訊號參數
	/// <para>只有在執行動作時呼叫才能取得參數</para>
	/// </summary>
	public class TCZRParameters
	{
		/// <summary>
		/// Pulse : 0~10000 ms
		/// </summary>
		public int T1 { get; set; }

		/// <summary>
		/// Pulse : 0~10000 ms
		/// </summary>
		public int T2 { get; set; }

		/// <summary>
		/// Pulse : 0~10000 ms
		/// </summary>
		public int T3 { get; set; }

		/// <summary>
		/// 0~2: 0=25%, 1=50%, 2=75%
		/// </summary>
		public int TDuty { get; set; }

		/// <summary>
		/// Special Pulse(Trillo) : 0~10000 ms
		/// </summary>
		public int TT1 { get; set; }

		/// <summary>
		/// Special Pulse(Trillo) : 0~10000 ms
		/// </summary>
		public int TT2 { get; set; }

		/// <summary>
		/// Special Pulse(Trillo) : 0~10000 ms
		/// </summary>
		public int TT3 { get; set; }

		/// <summary>
		/// 0~2: 0=25%, 1=50%, 2=75%
		/// </summary>
		public int TTDuty { get; set; }

		/// <summary>
		/// 0~ 1000000 Thermal image limit
		/// </summary>
		public int I2Tmax { get; set; }

		/// <summary>
		/// 0~ 60000 mV
		/// </summary>
		public int VinMin { get; set; }

		/// <summary>
		/// 0~ 60000 mV
		/// </summary>
		public int VinMax { get; set; }

		/// <summary>
		/// 0~ 60000 mA
		/// </summary>
		public int Imax { get; set; }

		/// <summary>
		/// 0~200, pluses needed to move to a position
		/// </summary>
		public int StepPulse { get; set; }

		/// <summary>
		/// 0~200 Extra pulses
		/// </summary>
		public int ExtraPulse { get; set; }

		/// <summary>
		/// 0=OK, -1 = timeout, -2=checksum error, -3= communication not started
		/// </summary>
		public string State { get; set; }
	}
}
