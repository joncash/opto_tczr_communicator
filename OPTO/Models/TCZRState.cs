﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OPTO.Models
{
	/// <summary>
	/// TCZR 狀態
	/// </summary>
	public class TCZRState
	{
		/// <summary>
		/// 焦段位置
		/// </summary>
		public int Position { get; set; }

		/// <summary>
		/// 1= 電壓太高, 0= OK
		/// </summary>
		public int ErrorOverSupply { get; set; }

		/// <summary>
		/// 1= 電壓太低, 0= OK
		/// </summary>
		public int ErrorUnderSupply { get; set; }

		/// <summary>
		/// 1= 電流太高, 0=OK
		/// </summary>
		public int ErrorOverCurrent { get; set; }

		/// <summary>
		/// 系統自我保護狀態 1= Error, 0=OK
		/// </summary>
		public int ErrorI2T { get; set; }

		/// <summary>
		/// 1= Error, 0=OK
		/// </summary>
		public int ErrorPosition { get; set; }

		/// <summary>
		/// 1 = Warning, reached with more steps than StepPulse, 0=OK
		/// </summary>
		public int WarningPosition { get; set; }

		/// <summary>
		/// 回傳步數
		/// </summary>
		public int StepDone { get; set; }

		/// <summary>
		/// 1= Busy, 0 = OK
		/// </summary>
		public int Busy { get; set; }

		/// <summary>
		/// 1 = Moving, 0 = OK
		/// </summary>
		public int Run { get; set; }

		/// <summary>
		/// OK, Timeout, Checksum Error, Communication not started
		/// </summary>
		public string State { get; set; }
	}
}
