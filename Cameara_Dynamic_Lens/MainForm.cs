﻿using OPTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using HalconDotNet;

namespace Cameara_Dynamic_Lens
{
	public partial class MainForm : Form
	{
		private TCZRManager _manager;
		public MainForm()
		{
			InitializeComponent();
			_manager = new TCZRManager();

			comboBox1.Text = "115200";
		}

		public int PortNumber
		{
			get
			{
				return Convert.ToInt32(numericUpDown1.Value);
			}
		}

		public int BaudRate
		{
			get
			{
				return Convert.ToInt32(comboBox1.Text);
			}
		}

		private void label1_Click(object sender, EventArgs e)
		{

		}

		private void button1_Click(object sender, EventArgs e)
		{
			//Connect
			var portNum = Convert.ToInt32(numericUpDown1.Value);
			var baud = Convert.ToInt32(comboBox1.Text);
			//MessageBox.Show(baud.ToString());
			var value = TCZR.InitComm(portNum, baud);
			var status = value == 0 ? "OK" : "Error";
			richTextBox1.AppendText("Connection Status : " + status + "\r\n");
		}

		private void button6_Click(object sender, EventArgs e)
		{
			//Status
			byte Position = 0, ErrorOverSupply = 0, ErrorUnderSupply = 0, ErrorOverCurrent = 0, ErrorI2T = 0, ErrorPosition = 0
				, WarningPosition = 0, StepDone = 0, Busy = 0, Run = 0;
			var val = TCZR.GetState(ref Position, ref ErrorOverSupply, ref ErrorUnderSupply, ref ErrorOverCurrent,
				ref ErrorI2T, ref ErrorPosition, ref WarningPosition, ref StepDone, ref Busy, ref Run);

			var status = getStatusByReturnValue(val);
			richTextBox1.AppendText("Device Status : " + status + "\r\n");

			richTextBox1.AppendText("Busy Status : " + Busy.ToString() + "\r\n");
			richTextBox1.AppendText("Run Status : " + Run.ToString() + "\r\n");

		}

		private void button2_Click(object sender, EventArgs e)
		{
			//0.25 倍 move to 0
			var val = TCZR.GoToPosition(0);
			var status = getStatusByReturnValue2(val);
			richTextBox1.AppendText("Move Status : " + status + "\r\n");
		}

		private void button3_Click(object sender, EventArgs e)
		{
			//0.5 倍 move to 1
			var val = TCZR.GoToPosition(1);
			var status = getStatusByReturnValue2(val);
			richTextBox1.AppendText("Move Status : " + status + "\r\n");
		}

		private void button4_Click(object sender, EventArgs e)
		{
			//1 倍 move to 2
			var val = TCZR.GoToPosition(2);
			var status = getStatusByReturnValue2(val);
			richTextBox1.AppendText("Move Status : " + status + "\r\n");
		}

		private void button5_Click(object sender, EventArgs e)
		{
			//2 倍 move to 3
			var val = TCZR.GoToPosition(3);
			var status = getStatusByReturnValue2(val);
			richTextBox1.AppendText("Move Status : " + status + "\r\n");
		}

		private void button8_Click(object sender, EventArgs e)
		{
			//關閉連線
			TCZR.CloseComm();
		}

		private void button7_Click(object sender, EventArgs e)
		{
			//取得版號
			var ver = TCZR.GetDllVersion();
			richTextBox1.AppendText("dll version : " + ver + "\r\n");
		}

		private void button9_Click(object sender, EventArgs e)
		{
			//設定 Timeout
			int timeout;
			if (!Int32.TryParse(textBox1.Text.Trim(), out timeout))
			{
				timeout = 250;
			}
			var val = TCZR.SetTimeOutRxCommand(timeout);
			var status = val == 0 ? "OK" : "Error";

			richTextBox1.AppendText("Set Timeout Status : " + status + "\r\n");
		}

		private void button10_Click(object sender, EventArgs e)
		{
			//firmware version
			var val = TCZR.GetFwVersion("1.0.0.0");
			var status = getStatusByReturnValue(val);
			richTextBox1.AppendText("FWVersion Status : " + status + "\r\n");
		}
		private string getStatusByReturnValue(int val)
		{
			var status = "";
			switch (val)
			{
				case 0:
					status = "OK";
					break;
				case -1:
					status = "Timeout";
					break;
				case -2:
					status = "Checksum error";
					break;
				case -3:
					status = "Communication not started";
					break;
			}
			return status;
		}
		private string getStatusByReturnValue2(int val)
		{
			var status = "";
			switch (val)
			{
				case 0:
					status = "OK";
					break;
				case -1:
					status = "Timeout";
					break;
				case -2:
					status = "Checksum error";
					break;
				case -3:
					status = "NAK device busy";
					break;
				case -4:
					status = "Communication not started";
					break;
			}
			return status;
		}

		private void button11_Click(object sender, EventArgs e)
		{
			int T1 = -1, T2 = -1, T3 = -1, TDuty = -1,
				TT1 = -1, TT2 = -1, TT3 = -1, TTDuty = -1,
				I2Tmax = -1, VinMin = -1, VinMax = -1, Imax = -1,
				StepPulse = -1, ExtraPulse = -1;

			var val = TCZR.GetParameters(ref T1, ref T2, ref T3, ref TDuty,
				ref TT1, ref TT2, ref TT3, ref TTDuty,
				ref I2Tmax,
				ref VinMin, ref VinMax, ref Imax,
				ref StepPulse, ref ExtraPulse);

			var status = getStatusByReturnValue(val);
			richTextBox1.AppendText("Parames Status : " + status + "\r\n");
		}

		private void button12_Click(object sender, EventArgs e)
		{
			var val = TCZR.Trillo();
			var status = getStatusByReturnValue2(val);
			richTextBox1.AppendText("Trillo Status : " + status + "\r\n");
		}

		private void button13_Click(object sender, EventArgs e)
		{
			var val = TCZR.DebugBufferTx();
			richTextBox1.AppendText("Tx Status : " + val + "\r\n");
		}

		private void button14_Click(object sender, EventArgs e)
		{
			var val = TCZR.DebugBufferRx();
			richTextBox1.AppendText("Rx Status : " + val + "\r\n");
		}

		private void button15_Click(object sender, EventArgs e)
		{
			_manager.SetBaudRate(BaudRate);
			_manager.SetPortNumber(PortNumber);

			if (!_manager.Connected)
			{
				var success = _manager.InitComm();
				setMessage(String.Format("Connection : {0}", (success) ? "OK" : "NG"));
			}
		}
		private void setMessage(string msg)
		{
			richTextBox1.AppendText(msg + "\r\n");
		}

		private void button16_Click(object sender, EventArgs e)
		{
			var model = _manager.GetState();
			setMessage("Stats : " + model.State);
		}

		private void button17_Click(object sender, EventArgs e)
		{
			_manager.CloseComm();
		}

		private void button18_Click(object sender, EventArgs e)
		{
			var val = _manager.GetDllVersion();
			setMessage("dll ver : " + val);
		}

		private void button19_Click(object sender, EventArgs e)
		{
			setMessage("fw OK : " + _manager.GetFwVersion());
		}

		private void button20_Click(object sender, EventArgs e)
		{
			var paras = _manager.GetParameters();
			var msg = (paras != null) ? paras.State + " T1 :" + paras.T1 : "Fail";
			setMessage("paras : " + msg);
		}

		private void button21_Click(object sender, EventArgs e)
		{
			var val = _manager.Trillo();
			setMessage("Trillo : " + val);
		}

		private void button22_Click(object sender, EventArgs e)
		{
			var timeout = Convert.ToInt32(textBox1.Text.Trim());
			var val = _manager.SetTimeOutRxCommand(timeout);
			setMessage("Set Timeout : " + val);
		}

		private void button23_Click(object sender, EventArgs e)
		{
			var position = Convert.ToInt32(numericUpDown2.Value);
			var val = _manager.GoToPosition(position);
			setMessage("Position : " + val);
		}
	}
}
